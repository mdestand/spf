import { mount } from 'vue-test-utils'
import Set from '@/components/viz/Set';

describe('Set.vue', () => {
  it('should render content if props are defined', () => {
    const vm = mount(Set, {
      propsData: {}
    });
    expect(vm.contains('.set')).to.equal(false);
    const vm2 = mount(Set, {
      propsData: {
          selectedGraph: { sets: [] },
          selectedSet: { paths: [], pathsGroups: [] },
          centerX: 100,
          centerY: 100,
          radius: 95
        }
    });
    expect(vm2.contains('.set')).to.equal(true);
  })
  it('should compute the angle span of a tangent', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.computeTangentAngleSpan(0)).to.deep.equal([90, 270]);
    expect(vm.vm.computeTangentAngleSpan(20)).to.deep.equal([110, 290]);
    expect(vm.vm.computeTangentAngleSpan(90)).to.deep.equal([180, 0]);
    expect(vm.vm.computeTangentAngleSpan(120)).to.deep.equal([210, 30]);
    expect(vm.vm.computeTangentAngleSpan(180)).to.deep.equal([270, 90]);
    expect(vm.vm.computeTangentAngleSpan(260)).to.deep.equal([350, 170]);
    expect(vm.vm.computeTangentAngleSpan(270)).to.deep.equal([0, 180]);
    expect(vm.vm.computeTangentAngleSpan(300)).to.deep.equal([30, 210]);
  })
  it('should crop angle span', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.cropAngleSpan([10, 190], 30, 30)).to.deep.equal([40, 160]);
    expect(vm.vm.cropAngleSpan([350, 170], 30, 30)).to.deep.equal([20, 140]);
    expect(vm.vm.cropAngleSpan([200, 20], 30, 30)).to.deep.equal([230, 350]);
  })
  it('should compute steps for angle span', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.computeSpanSteps([40, 160], 2)).to.deep.equal([160, 40]);
    expect(vm.vm.computeSpanSteps([40, 160], 4)).to.deep.equal([160, 120, 80, 40]);
    expect(vm.vm.computeSpanSteps([20, 140], 3)).to.deep.equal([140, 80, 20]);
    expect(vm.vm.computeSpanSteps([330, 90], 3)).to.deep.equal([90, 30, 330]);
  })
  it('should return circles steps according to requested depth', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    const circleContainer = { center: { x: 10, y: 10 }, radius: 10 }
    const circleSet = { center: { x: 12.25, y: 7.35 }, radius: 3 }
    console.log('toto', vm.vm.computeCircleSteps(circleContainer, circleSet, 45, 3)[0])
    console.log('toto', vm.vm.computeCircleSteps(circleContainer, circleSet, 45, 3)[1])
    console.log('toto', vm.vm.computeCircleSteps(circleContainer, circleSet, 45, 3)[2])
    /*console.log('titi', 
      { center: { x: 14, y: 14 }, radius: 4.75 }
    )*/
    expect(vm.vm.computeCircleSteps(circleContainer, circleSet, 45, 3)).to.deep.equal([
      { center: { x: 14, y: 14 }, radius: 3.6666666666666665 },
      { center: { x: 14, y: 14 }, radius: 4.333333333333333 },
      { center: { x: 14, y: 14 }, radius: 5 }
    ]);
  })
  it('should return the slope of a line', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.findSlope({x:4,y:3}, {x:1,y:2})).to.equal(1/3);
    expect(vm.vm.findSlope({x:3,y:4}, {x:5,y:1})).to.equal(-1*(3/2));
    expect(vm.vm.findSlope({x:12,y:4}, {x:12,y:1})).to.equal(0);
  })
  it('should return y-intercept from point + slope', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.findYIntercept(1/3, {x:4,y:3})).to.equal(1.67);
  })
  it('should return y from point + slope + x', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.findY(1/3, {x:4,y:3}, 0)).to.equal(1.67);
    expect(vm.vm.findY(1/3, {x:4,y:3}, 1)).to.equal(2);
    expect(vm.vm.findY(-1, {x:-2,y:3}, 2)).to.equal(-1);
  })
  it('should return intersections between a circle and a line', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.findCircleLineIntersections({ center: { x: 0, y: 0 }, radius: 2 }, { x:4, y:3 }, { x:1, y:2 }))
      .to.deep.equal([{x:0.66,y:1.89}, {x:- 1.66,y:1.11}]);
    expect(vm.vm.findCircleLineIntersections({ center: { x: 0, y: 0 }, radius: 2 }, { x:0, y:2 }, { x:2, y:0 }))
      .to.deep.equal([{x:2,y:0}, {x:0,y:2}]);
    expect(vm.vm.findCircleLineIntersections({ center: { x: 13, y: 7 }, radius: 7 }, { x:15, y:5 }, { x:15, y:2 }))
      .to.deep.equal([{x: 19.71, y: 5}, {x: 6.29, y: 5}]);
  })

  it('should return length of line segment between 2 points', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm. findLineLength({ x:4, y:3 }, { x:1, y:2 }))
      .to.deep.equal(3.16);
  })
  it('should return relevant intersection from circleSet', () => {
    const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    expect(vm.vm.findCircleLineRelevantIntersection([{ x:4, y:3 }, { x:1, y:2 }], { center: { x: 1, y: 1 } }))
      .to.deep.equal({ x:4, y:3 });

  })
  it('should return paths definition', () => {
    /*const vm = mount(Set, {
      propsData: {
        selectedGraph: { sets: [] },
        selectedSet: { paths: [], pathsGroups: [] },
        centerX: 100,
        centerY: 100,
        radius: 95
      }
    });
    
    expect(vm.vm.computePaths(
      [
        { depth: 1 },
        { depth: 2 },
        { depth: 3 }
      ],
      [180, 240, 300],
      { center: { x: 10, y: 10 }, radius: 10 },
      { center: { x: 15, y: 5 }, radius: 3 },
      [
        { center: { x: 14, y: 6 }, radius: 5 },
        { center: { x: 13, y: 7 }, radius: 7 },
        { center: { x: 12, y: 8 }, radius: 9 }
      ]))
      .to.deep.equal([
        'M22,5 L9.81,3.27',
        'M19,11 L16.73,10.19 L18.87,10.81',
        'M12,11 L10.46,9.54 L10.7,13.6 L5.64,14.36'       
      ]);
    */
  })
})
