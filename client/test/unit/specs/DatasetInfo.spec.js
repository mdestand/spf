import { mount } from 'vue-test-utils'
import DatasetInfo from '@/components/viz/DatasetInfo';
//import data from '../data/data';

describe('DatasetInfo.vue', () => {
  it('should render content if selectedBubble prop is defined', () => {
    const vm = mount(DatasetInfo, {
      propsData: {
        selectedBubble: null,
        selectedGraph: null,
        radius: 10,
        width: 800,
        height: 600
      }
    });
    //console.log('o', vm)
    expect(vm.contains('.rectDatasetInfo')).to.equal(false);
    const vm2 = mount(DatasetInfo, {
      propsData: {
        selectedBubble: {name: 'toto'},
        selectedGraph: null,
        radius: 10,
        width: 800,
        height: 600
      }
    });
    expect(vm2.contains('.rectDatasetInfo')).to.equal(true);
  })
})
