import { mount } from 'vue-test-utils'
import Cloud from '@/components/viz/Cloud';
import data from '../data/data';

describe('Cloud.vue', () => {
  it('should render content if datacloud is defined and not empty', () => {
    const vm = mount(Cloud, {
      propsData: {
        datacloud: {}
      }
    });
    expect(vm.contains('.cloud')).to.equal(false);
    const vm2 = mount(Cloud, {
      propsData: {
        datacloud: data
      }
    });
    expect(vm2.contains('.cloud')).to.equal(true);
  })
})
