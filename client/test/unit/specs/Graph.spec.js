import { mount } from 'vue-test-utils'
import Graph from '@/components/viz/Graph';

describe('Graph.vue', () => {
  it('should compute the angle of line going through a point in the plan', () => {
    const vm = mount(Graph, {
      propsData: {
          selectedGraph: { sets: [] },
          centerX: 100,
          centerY: 100,
          radius: 95,
          provenance: {},
          transitionEnded: false
        }
    });
    expect(vm.vm.computeAngle({x: 100, y: 100}, {x: 200, y: 100})).to.equal(0);
    expect(vm.vm.computeAngle({x: 100, y: 100}, {x: 200, y: 0})).to.equal(315);
    expect(vm.vm.computeAngle({x: 100, y: 100}, {x: 100, y: 0})).to.equal(270);
    expect(vm.vm.computeAngle({x: 100, y: 100}, {x: 0, y: 100})).to.equal(180);
    expect(vm.vm.computeAngle({x: 100, y: 100}, {x: 100, y: 200})).to.equal(90);
    expect(vm.vm.computeAngle({x: 100, y: 100}, {x: 200, y: 200})).to.equal(45);
    
  })
  it('should compute the coords of a points on a circle given the center of the cicle, its radius, and an angle', () => {
    const vm = mount(Graph, {
      propsData: {
          selectedGraph: { sets: [] },
          centerX: 100,
          centerY: 100,
          radius: 95,
          provenance: {},
          transitionEnded: false
        }
    });
    expect(vm.vm.computePointOnCircle({x: 100, y: 100}, 100, 0)).to.deep.equal({x: 200, y: 100});
    expect(vm.vm.computePointOnCircle({x: 100, y: 100}, 100, 45)).to.deep.equal({x: 171, y: 171});
    expect(vm.vm.computePointOnCircle({x: 100, y: 100}, 100, 90)).to.deep.equal({x: 100, y: 200});
    expect(vm.vm.computePointOnCircle({x: 100, y: 100}, 100, 180)).to.deep.equal({x: 0, y: 100});
  })
})
