export default {
  europeana: {
    _id: "europeana-sparql",
    website: null,
    full_download: [],
    domain: "cross_domain",
    identifier: "europeana-sparql",
    triples: " 3798446742",
    links: [
      {
        target: "geonames",
        value: "1650634"
      }
    ]
  }
}

/*export default {
  "europeana-sparql": {
    "_id": "europeana-sparql",
    "website": null,
    "full_download": [],
    "domain": "cross_domain",
    "identifier": "europeana-sparql",
    "triples": " 3798446742",
    "links": [
      {
        "target": "geonames",
        "value": "1650634"
      }
    ],
    "license": "http://www.opendefinition.org/licenses/cc-zero",
    "title": "Europeana SPARQL",
    "image": "",
    "sparql": [
      {
        "status": "OK",
        "access_url": "http://europeana.ontotext.com/sparql",
        "description": "",
        "title": "SPARQL endpoint"
      }
    ],
    "namespace": "http://data.europeana.eu/item/",
    "other_download": [
      {
        "status": "OK",
        "description": "",
        "title": "EDM schema",
        "mirror": [
          "http://download.lodlaundromat.org/d6fc6c9d32742266faadc0163795a444"
        ],
        "media_type": "meta/rdf-schema",
        "access_url": "http://www.europeana.eu/schemas/edm/"
      },
      {
        "status": "OK",
        "description": "",
        "title": "VOID description",
        "mirror": [
          "http://download.lodlaundromat.org/4d26fb4d2e5e1b065c435979858679f7"
        ],
        "media_type": "meta/void",
        "access_url": "http://data.europeana.eu/void.ttl"
      }
    ],
    "keywords": [
      "culturalheritage",
      "culture",
      "edm",
      "europeana",
      "europeana data model",
      "format-edm",
      "lod",
      "no-vocab-mappings",
      "published-by-third-party",
      "sparql",
      "sparql-endpoint",
      "topic.media"
    ],
    "doi": "",
    "owner": "",
    "contact_point": {
      "email": "info@ontotext.com",
      "name": "Ontotext"
    },
    "example": [
      {
        "status": "OK",
        "media_type": "text/html; charset=UTF-8",
        "access_url": "http://europeana.ontotext.com/europeana/tab?uri=http%3A%2F%2Fdata.europeana.eu%2Fitem%2F01004%2FE17BBC456775874D778D9540864548CF298254C4&role=CHO",
        "description": "",
        "title": "sample object (all triples in HTML)"
      },
      {
        "status": "OK",
        "media_type": "text/html; charset=UTF-8",
        "access_url": "http://europeana.ontotext.com/download/n3?uri=http://data.europeana.eu/item/01004/E17BBC456775874D778D9540864548CF298254C4&format=n3&inference=all",
        "description": "",
        "title": "sample object (all triples in Turtle)"
      },
      {
        "status": "OK",
        "media_type": "text/html; charset=UTF-8",
        "access_url": "http://europeana.ontotext.com/europeana/tab?uri=http%3A%2F%2Fdata.europeana.eu%2Fitem%2F01004%2FE17BBC456775874D778D9540864548CF298254C4&role=Graph&inference=all",
        "description": "",
        "title": "sample object (EDM graph visualization)"
      }
    ],
    "description": {
      "en": "Europeana EDM data loaded to OWLIM, with SPARQL and visualization. \r\nContinuing development as part of @eCreativeEU"
    },
    "collections": [
      {"name": "skos:Concept", "triples": 395297},
      {"name": "frbr-rda:Work", "size": 80991},
      {"name": "bnffr:expositionVirtuelle", "size":3818},
      {"name": "foaf:Person", "size": 165722},
      {"name": "foaf:Organization", "size": 38450},
			{"name": "foaf:Document", "size": 4218},
      {"name": "skos:Concept", "size": 50000}
    ],
    "graphs": [
      {
        "name": "urn:namedgraph1",
        "triples": 395297,
        "sets": [
          {
            "type": ["skos:Concept"],
            "triples": 395297,
            "prov": {
              "startedAtTime": 1224720000000

            },
            "paths": [
              {
                "depth": 1,
                "number": 2003,
                "lastLinks": [
                  {
                    "target": "dbpedia",
                    "value": "14"
                  },
                  {
                    "target": "dbpedia",
                    "value": "28"
                  }
                ]
              },
              {
                "depth": 2,
                "number": 345,
                "lastLinks": [
                  {
                    "target": "dbpedia",
                    "value": "7379"
                  },
                  {
                    "target": "dbpedia",
                    "value": "7379"
                  }
                ]
              },
              {
                "depth": 3,
                "number": 152,
                "lastLinks": [
                  {
                    "target": "dbpedia",
                    "value": "7379"
                  },
                  {
                    "target": "dbpedia",
                    "value": "7379"
                  }
                ]
              }
            ]
          },
          {
            "type": ["frbr-rda:Work"],
            "triples": 80991,
            "prov": {
              "startedAtTime": 1224720000000
            }
          },
          {
            "type": ["bnffr:expositionVirtuelle"],
            "triples":3818,
            "prov": {
              "startedAtTime": 1056326400000
            }
          },
          {
            "type": ["foaf:Person"],
            "triples": 165722,
            "prov": {
              "startedAtTime": 25488000000
            }
          },
          {
            "type": ["foaf:Organization"],
            "triples": 38450,
            "prov": {
              "startedAtTime": 1224720000000
            }
          },
          {
            "type": ["foaf:Document"],
            "triples": 4218,
            "prov": {
              "startedAtTime": 972259200000
            }
          },
          {
            "type": ["skos:Concept", "frbr-rda:Work"],
            "triples": 50000,
            "prov": {
              "startedAtTime": 884563200000
            }
          }
        ]
      },
      {"name": "urn:namedgraph2", "triples": 80991},
      {"name": "urn:namedgraph3", "triples":3818},
      {"name": "urn:namedgraph4", "triples": 165722},
      {"name": "urn:namedgraph5", "triples": 38450},
			{"name": "urn:namedgraph6", "triples": 4218},
      {"name": "urn:namedgraph7", "triples": 50000}
    ]
  },
  "ww1lod": {
    "_id": "ww1lod",
    "website": null,
    "full_download": [],
    "domain": "publications",
    "identifier": "ww1lod",
    "triples": "40160",
    "links": [
      {
        "target": "dbpedia",
        "value": "152"
      },
      {
        "target": "geonames-semantic-web",
        "value": "1248"
      },
      {
        "target": "muninn",
        "value": "11"
      },
      {
        "target": "pcdhn",
        "value": "29"
      }
    ],
    "license": "http://www.opendefinition.org/licenses/cc-by-sa",
    "title": "World War 1 as Linked Open Data",
    "image": "",
    "sparql": [
      {
        "status": "OK",
        "access_url": "http://ldf.fi/ww1lod/sparql",
        "description": "",
        "title": "SPARQL endpoint"
      }
    ],
    "namespace": "http://ldf.fi/ww1lod/",
    "other_download": [
      {
        "status": "OK",
        "media_type": "text/turtle; charset=utf-8",
        "access_url": "http://ldf.fi/ww1lod/6db396b8",
        "description": "",
        "title": "Example resource"
      },
      {
        "status": "OK",
        "media_type": "text/turtle; charset=utf-8",
        "access_url": "http://ldf.fi/ww1lod/",
        "description": "",
        "title": "Home page"
      },
      {
        "status": "OK",
        "media_type": "text/turtle",
        "access_url": "http://ldf.fi/ww1lod/void/",
        "description": "",
        "title": "VoID Description"
      }
    ],
    "keywords": [
      "culturalheritage",
      "deref-vocab",
      "format-cidoc-crm",
      "format-foaf",
      "format-geo",
      "format-georss",
      "format-org",
      "format-rdf",
      "format-schema",
      "format-skos",
      "history",
      "lod",
      "world-war-one"
    ],
    "doi": "",
    "owner": "",
    "contact_point": {
      "email": "",
      "name": ""
    },
    "example": [],
    "description": {
      "en": "This dataset contains strictly quality-controlled rich information on events, actors and places related to the First World War. As such, it is meant to be used as a reference dataset to which other datasets (e.g. museum or library collections dealing with WW1 topics) can be linked."
    }
  }
}*/
