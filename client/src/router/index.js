import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css'
import Router from 'vue-router';
import Wrap from '@/components/viz/Wrap';
import Admin from '@/components/analysis/Wrap';

Vue.use(Vuetify, {
  iconfont: 'mdi'
});
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Wrap',
      component: Wrap,
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
    },
  ],
});
