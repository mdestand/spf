import express from 'express'
import { SparqlClient } from 'sparql-client-2'
import fetch from 'node-fetch'
import fs from 'fs.promised'
import path from 'path'
import readFilePromise from 'fs-readfile-promise'
const router = express.Router()

const maxQueries = 10

router.post('/types', (req, res, next) => {
  loadTypes(req.body.graphquery, req.body.endpoint)
    .then(result =>{
      res.json(result)
      res.end()
    })
})

router.post('/graphs', (req, res, next) => {
  console.log('get graphs', req.body.endpoint)
  loadGraphs(req.body.endpoint)
    .then(result => {
      //console.log('res', result)
      res.json(result)
      res.end()
    })
})

router.post('/paths', (req, res, next) => {
  console.log('get paths', req.body.endpoint)
  req.setTimeout(0)
  loadPaths(req.body.corese, req.body.filename, req.body.graphquery, req.body.endpoint, req.body.type, req.body.total, req.body.maxlevel)
    .then(result =>{
      res.json(result)
      res.end()
    })
})

router.post('/countjoints', (req, res, next) => {
  console.log('get count joints', req.body.endpoint)
  req.setTimeout(0)
  loadCountjoints(req.body.corese, req.body.filename, req.body.graphquery, req.body.endpoint, req.body.distantEndpoint, req.body.type, req.body.total, req.body.maxlevel)
    .then(result =>{
      console.log('count joints', result)
      res.json(result)
      res.end()
    })
})

router.post('/followpaths', (req, res, next) => {
  console.log('get follow joints', req.body.endpoint)
  req.setTimeout(0)
  loadFollowpaths(req.body.corese, req.body.filename, req.body.graphquery, req.body.endpoint, req.body.distantEndpoint, req.body.type, req.body.total, req.body.maxlevel)
    .then(result =>{
      console.log('follow joints', result)
      res.json(result)
      res.end()
    })
})

router.post('/intermediatetypes', (req, res, next) => {
  console.log('get intermediate types', req.body.endpoint)
  req.setTimeout(0)
  loadIntermediateTypes(req.body.paths, req.body.graph, req.body.endpoint, req.body.type)
    .then(result =>{
      console.log(result)
      res.json(result)
      res.end()
    })
})

router.post('/languages', (req, res, next) => {
  console.log('get languages', req.body.endpoint)
  req.setTimeout(0)
  loadLanguages(req.body.paths, req.body.graph, req.body.endpoint, req.body.type)
    .then(result =>{
      console.log(result)
      res.json(result)
      res.end()
    })
})


router.post('/followgraph', (req, res, next) => {
  console.log('get follow joints', req.body.endpoint)
  req.setTimeout(0)
  loadFollowGraph(req.body.corese, req.body.filename, req.body.graphquery, req.body.endpoint, req.body.distantGraph, req.body.type, req.body.total, req.body.maxlevel)
    .then(result =>{
      console.log('follow graph', result)
      res.json(result)
      res.end()
    })
})

const deleteFiles = async (filename) => {
  try {  
    await fs.unlink(filename + '.json')
    await fs.unlink(filename + '.ttl')
    return
  }
  catch(error) {
    console.error('no such files', error);
  }
}
const loadPaths = async (corese, filename, graphquery, endpoint, type, total, maxlevel) => {
  let fullPath = path.join(__dirname, filename)
  await deleteFiles(fullPath)

  let query = `@ldpath <${endpoint}@${maxlevel}>
  @file <${path.join(__dirname, filename)}>
  ${graphquery}
  @split 10
  @trace
    select *
    where {
      ?s1 rdf:type <${type.replace('#', '%23')}>
    }`
    console.log(query)
  // does not send the results, only a signal that it has been written in a file
  await fetch(`${corese}?query=${(query)}`)
  // console.log('reponse corese ok')   
  await new Promise(resolve => setTimeout(resolve, 2000))
  let data = await readFilePromise(fullPath + '.json', 'utf8')
  // console.log('ok results', data)
  let parsed = JSON.parse(data)
  let results = []
  parsed['@graph'].forEach(path => {
    if (path['ns1:type']){
      //console.log(path['ns1:subject']['@value'], total)
      results.push( {
        entrypoint: path['ns1:type']['@id'],
        category: path['ns1:datatype'] ? path['ns1:datatype']['@id'] : 'none (URI)',
        coverage: Math.round(Number(path['ns1:subject']['@value']) / total * 10000)/100,
        level: path['ns1:path']['@list'].length,
        readablePath: path['ns1:path']['@list'].map(p => { return { uri: p['@id'] } }),
        joints: [],
        countjoints: [],
        entities: path['ns1:subject'] ? path['ns1:subject']['@value'] : null,
        total: path['ns1:count'] ? path['ns1:count']['@value'] : null,
        unique: path['ns1:distinct'] ? path['ns1:distinct']['@value'] : null,
        avg: path['ns1:avg']? path['ns1:avg']['@value'] : null,
        charlength: path['ns1:length'] ? path['ns1:length']['@value'] : null,
        min: path['ns1:min'] ? path['ns1:min']['@value'] : null,
        max: path['ns1:max'] ? path['ns1:max']['@value'] : null
      })
    }
  })
  //console.log('parsed', parsed)
  //console.log('results', results)
  return { paths: results }
}

const loadCountjoints = async (corese, filename, graphquery, endpoint, distantEndpoint, type, total, maxlevel) => {
  let fullPath = path.join(__dirname, filename)
  await deleteFiles(fullPath)

  let query = `@ldpath <${endpoint}@${maxlevel}> <${distantEndpoint}>
  @file <${path.join(__dirname, filename)}>
  ${graphquery}
  @split 10
  @timeout 100000 @slice 3
  @trace
  select *
  where {
    ?s1 rdf:type <${type.replace('#', '%23')}>
  }`

  
  // does not send the results, only a signal that it has been written in a file
  await fetch(`${corese}?query=${(query)}`)
  // console.log('reponse corese ok')   
  await new Promise(resolve => setTimeout(resolve, 2000))
  let data = await readFilePromise(path.join(__dirname, filename+ '.json'), 'utf8')
  // console.log('ok results', data)
  let parsed = JSON.parse(data)
  let results = []
  parsed['@graph'].forEach(path => {
    if (path['ns1:endpoint'] && path['ns1:count']){
      results.push( {
        readablePath: path['ns1:path']['@list'].map(p => { return { uri: p['@id'] } }),
        extensions: [],
        endpoint: distantEndpoint,
        jointtype: 'dataset',
        countJoints: path['ns1:count'] ? path['ns1:count']['@value'] : 0/*,
        entities: path['ns1:subject']*/
      })
    }
  })
  // console.log('parsed', parsed)
  console.log('results count joints', results)
  return { countjoints: results }
}

const loadFollowpaths = async (corese, filename, graphquery, endpoint, distantEndpoint, type, total, maxlevel) => {
  let fullPath = path.join(__dirname, filename)
  await deleteFiles(fullPath)

  let query = `@ldpath <${endpoint}@${maxlevel}> <${distantEndpoint}@1>
  @file <${path.join(__dirname, filename)}>
  ${graphquery}
  @split 10
  @timeout 100000 @slice 3
  @trace
    select *
    where {
      ?s1 rdf:type <${type.replace('#', '%23')}>
    }`
  // does not send the results, only a signal that it has been written in a file
  console.log('on continue', query)  
  await fetch(`${corese}?query=${(query)}`)
  console.log('reponse corese ok')   
  await new Promise(resolve => setTimeout(resolve, 2000))
  let data = await readFilePromise(fullPath + '.json', 'utf8')
  //data = String.raw({raw: data})
  // console.log('ok results', data)
  //data = data.replace(/[\u0000-\u0019]+/g,"")
  data = data.replace(/IT\\ICCU\\CFIV\\/g, "IT\\\\ICCU\\\\CFIV\\\\")
  //console.log('ok2 results', data)
  let parsed = JSON.parse(data)
  let results = []
  parsed['@graph'].forEach(path => {
    if (path['ns1:path2']){
      results.push( {
        category: path['ns1:datatype'] ? path['ns1:datatype']['@id'] : 'none (URI)',
        coverage: Math.round(Number(path['ns1:subject']['@value']) / total * 10000)/100,
        level: 1,
        readablePath: path['ns1:path']['@list'].map(p => { return { uri: p['@id'] } }),
        extension: path['ns1:path2']['@list'].map(p => { return { uri: p['@id'] } }),
        entities: path['ns1:subject'] ? path['ns1:subject']['@value'] : null,
        endpoint: distantEndpoint,
        total: path['ns1:count'] ? path['ns1:count']['@value'] : null,
        unique: path['ns1:distinct'] ? path['ns1:distinct']['@value'] : null,
        avg: path['ns1:avg']? path['ns1:avg']['@value'] : null,
        charlength: path['ns1:length'] ? path['ns1:length']['@value'] : null,
        min: path['ns1:min'] ? path['ns1:min']['@value'] : null,
        max: path['ns1:max'] ? path['ns1:max']['@value'] : null
      })
    }
  })
  //console.log('parsed', parsed)
  //console.log('results', results)
  return { joints: results }
}

const loadFollowGraph = async (corese, filename, graphquery, endpoint, distantGraph, type, total, maxlevel) => {
  /*let fullPath = path.join(__dirname, filename)
  await deleteFiles(fullPath)

  let query = `@ldpath <${endpoint}@${maxlevel}> <${distantEndpoint}@1>
  @file <${path.join(__dirname, filename)}>
  @split 10
  @trace
    select * ${graphquery}
    where {
      ?s1 rdf:type <${type.replace('#', '%23')}>
    }`
  // does not send the results, only a signal that it has been written in a file
  console.log('on continue')   
  await fetch(`${corese}?query=${(query)}`)
  console.log('reponse corese ok')   
  await new Promise(resolve => setTimeout(resolve, 2000))
  let data = await readFilePromise(fullPath + '.json', 'utf8')
  // console.log('ok results', data)
  let parsed = JSON.parse(data)
  let results = []
  parsed['@graph'].forEach(path => {
    if (path['ns1:path2']){
      results.push( {
        category: path['ns1:datatype'] ? path['ns1:datatype']['@id'] : 'URI',
        coverage: path['ns1:subject'] / total * 100,
        level: 1,
        readablePath: path['ns1:path']['@list'].map(p => { return { uri: p['@id'] } }),
        extension: path['ns1:path2']['@list'].map(p => { return { uri: p['@id'] } }),
        entities: path['ns1:subject'],
        total: path['ns1:count'],
        unique: path['ns1:distinct'],
        endpoint: distantEndpoint,
        avg: path['ns1:avg'] || null,
        charlength: path['ns1:charlength'] || null,
        min: path['ns1:min'] || null,
        max: path['ns1:max'] || null
      })
    }
  })
  //console.log('parsed', parsed)
  //console.log('results', results)
  return { joints: results }*/
}


const loadGraphs = async (endpoint) => {
  let graphsquery = `SELECT DISTINCT ?g
  WHERE {
    GRAPH ?g { ?s ?p ?o }
  } group by ?g`
  console.log(graphsquery )
  let graphs = await getData(endpoint, graphsquery, {})
  graphs = graphs.results.bindings.map(graph => graph.g.value)
  console.log(graphs)
  let results = []
  let graphQueries = graphs.map(g =>  `SELECT (COUNT(*) as ?triples) FROM <${g}> WHERE { GRAPH ?g{ ?s ?p ?o} } GROUP BY ?g`)
  for (let i = 0; i < graphs.length; i += maxQueries) {  
    let elementsToSlice = (graphs.length - i < maxQueries) ? graphs.length - i : maxQueries
    let triples = await Promise.all(graphQueries
      .slice(i, i + elementsToSlice)
      .map(triplesquery => {
        return getData(endpoint, triplesquery, {})
      })
      .map((promise, index) => promise.catch(e => {
        console.error('Error getting number of triples in graph', graphs[index] ,e)
        return undefined
      }))
    )
    for (let j = 0; j < triples.length; j ++) {
      results.push({
        name: graphs[i+j],
        triples: triples[j] ? Number(triples[j].results.bindings[0].triples.value) : null,
        sets: []
      })
    }
  }
  return { graphs: results }
}

const loadTypes = async (graphquery, endpoint) => {
  //return Promise.resolve()
    //.then(() => {
  let typesquery = `SELECT DISTINCT ?type ${graphquery} WHERE { ?s rdf:type ?type }`
  let types = await getData(endpoint, typesquery, {})
  types = types.results.bindings.map(type => type.type.value)
  let results = []
  let typesQueries = types.map(t=>  `SELECT (COUNT(DISTINCT ?s) as ?occ) ${graphquery} WHERE { ?s rdf:type <${t}> }`)
  for (let i = 0; i < types.length; i += maxQueries) {
    let elementsToSlice = (types.length - i < maxQueries) ? types.length - i : maxQueries
    let occs = await Promise.all(typesQueries
      .slice(i, i + elementsToSlice)
      .map(triplesquery => {
        return getData(endpoint, triplesquery, {})
      })
      .map((promise, index) => promise.catch(e => {
        console.error('Error getting number of occurences for type', types[index] ,e)
        return undefined
      }))
    )      
    for (let j = 0; j < occs.length; j ++) {
      results.push({
        type: types[i+j],
        entities: occs[j] ? Number(occs[j].results.bindings[0].occ.value): null,
        paths: [],
        jointsSummary: []
      })
    }
  }

  return { types: results }
}


const makeTypeQuery = (entrypointType, pathParts, graphs, level) => {
  const entrypointName = 's'
  const propName = 'o'
  const graph = graphs ? `FROM <${graph}> ` : ``
  let query = `?${entrypointName} rdf:type <${entrypointType}> . `
  let selected = ``
  let group = ``
  let prevSubjects = []
  for (let index = 0; index < pathParts.length; index ++) {
    let predicate = pathParts[index].uri
    let thisSubject = (index === 0) ? entrypointName : `${propName}inter${index-1}`
    let thisObject = (index === (pathParts.length-1)) ? propName : `${propName}inter${index}`
    query = query.concat(`?${thisSubject} <${predicate}> ?${thisObject} . `)
    //if (index+1 === level) {
      selected = selected.concat(` ?type${index} `) //(COUNT(?${thisObject}) AS ?counttype${thisObject})
      group = group.concat(`?type${index} `)
      query = query.concat(`?${thisObject} rdf:type ?type${index} . `) 
    //}
    prevSubjects.push(thisSubject)
    
  }
  return `SELECT DISTINCT ${selected} ${graph} WHERE { ${query} } `

}
const loadIntermediateTypes = async (paths, graphs, endpoint, entrypointType) => {
  let typesQueries = paths.map(t =>  makeTypeQuery(entrypointType, t.readablePath, graphs))
  //console.log(paths[0])
  //console.log(typesQueries)
  let results = []
  for (let i = 0; i < paths.length; i += maxQueries) {
    let elementsToSlice = (paths.length - i < maxQueries) ? paths.length - i : maxQueries
    let occs = await Promise.all(typesQueries
      .slice(i, i + elementsToSlice)
      .map(triplesquery => {
        return getData(endpoint, triplesquery, {})
      })
      .map((promise, index) => promise.catch(e => {
        console.error('Error getting intermediate datatypes', paths[index] ,e)
        return undefined
      }))
    )      
    for (let j = 0; j < occs.length; j ++) {
      if (occs[j]) {

        let level = paths[i+j].level 
        let types = Array.from({length: level}, v => [])

        occs[j].results.bindings.forEach(res => {
          for (let k = 0; k < level; k ++) {
            if(res['type'+k] && 
              res['type'+k].value !== "http://www.w3.org/2000/01/rdf-schema#Class" && 
              !types[k].includes(res['type'+k].value)) {
              types[k].push(res['type'+k].value)
            }
          }
        })
        let path = paths[i+j]
        path.types = types
        results.push(path)
      } else {
        results.push(paths[i+j])
      }
    }
  }
  return { paths: results }
}

const makeLanguageQuery = (entrypointType, pathParts, graphs, level) => {
  const entrypointName = 's'
  const propName = 'o'
  const graph = graphs ? `FROM <${graph}> ` : ``
  let query = `?${entrypointName} rdf:type <${entrypointType}> . `
  let group = ``
  let prevSubjects = []
  for (let index = 0; index < pathParts.length; index ++) {
    let predicate = pathParts[index].uri
    let thisSubject = (index === 0) ? entrypointName : `${propName}inter${index-1}`
    let thisObject = (index === (pathParts.length-1)) ? propName : `${propName}inter${index}`
    query = query.concat(`?${thisSubject} <${predicate}> ?${thisObject} . `)
    prevSubjects.push(thisSubject)
    
  }
  return `SELECT DISTINCT ?language ${graph} WHERE { ${query} BIND ( lang(?${propName}) AS ?language ) } `

}
const loadLanguages = async (paths, graphs, endpoint, entrypointType) => {
  let typesQueries = paths.map(t =>  makeLanguageQuery(entrypointType, t.readablePath, graphs))
  //console.log(paths[0])
  console.log(typesQueries)
  let results = []
  for (let i = 0; i < paths.length; i += maxQueries) {
    let elementsToSlice = (paths.length - i < maxQueries) ? paths.length - i : maxQueries
    let occs = await Promise.all(typesQueries
      .slice(i, i + elementsToSlice)
      .map(triplesquery => {
        return getData(endpoint, triplesquery, {})
      })
      .map((promise, index) => promise.catch(e => {
        console.error('Error getting languages', paths[index] ,e)
        return undefined
      }))
    )      
    for (let j = 0; j < occs.length; j ++) {
      if (occs[j]) {
        let languages = []
        occs[j].results.bindings.forEach(res => {
          console.log(res['language'])
          if(res['language'] && res['language'].value !== '') {
            languages.push(res['language'].value)
          }
        })
        let path = paths[i+j]
        path.languages = languages
        results.push(path)
      } else {
        results.push(paths[i+j])
      }
    }
  }
  return { paths: results }
}

const getData = (endpoint, query, prefixes) => {
  // console.log(query)
  const client = new SparqlClient(endpoint, {
      requestDefaults: {
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Accept': 'application/sparql-results+json,application/json'
          }
      }
  })
      .registerCommon('rdf', 'rdfs')
      .register(prefixes)
  return client
      .query(query)
      .execute()
}
export default router
