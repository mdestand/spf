import express from 'express'
import fs from 'fs'
import path from 'path'
import analyse from './analyse'

let router = express.Router()



// middleware to use for all requests
router.use('/', (req, res, next) => {
    if (req.path === '/favicon.ico') {
        res.writeHead(200, { 'Content-Type': 'image/x-icon' })
        res.end()
    }
    next()
})


router.post('/write', (req, res, next) => {
  
  if (!req.body.filecontent || !req.body.filename) {
    console.error('You must provide the name and content of the file')
    res.end()
  } else {
    let filecontent = req.body.filecontent
    let preffilecontent = JSON.stringify(filecontent)
    filecontent.prefixes = req.body.prefixes
    filecontent.properties = req.body.properties
    fs.writeFile(path.join(__dirname, req.body.filename + '.json'), JSON.stringify(filecontent), 'utf8', (err) => { 
        if(err) console.error('Error writing the file ', err)
    })
    for(let prefix in req.body.prefixes) {
      let url = req.body.prefixes[prefix].replace(new RegExp("/", "g"), "\/")
      let reg = new RegExp(url, "g")
      preffilecontent = preffilecontent.replace(reg, prefix + ':')
    }
    preffilecontent = JSON.parse(preffilecontent)
    preffilecontent.prefixes = req.body.prefixes
    preffilecontent.properties = req.body.properties
    preffilecontent.graphs = preffilecontent.graphs.map(g => {      
      g.sets = g.sets.map(set => {
        return (set.paths.length > 0) ? set : undefined
      }).filter(set => set)
      return (g.sets.length > 0) ? g : undefined
    }).filter(g => g)
    fs.writeFile(path.join(__dirname, req.body.filename + '_pref.json'), JSON.stringify(preffilecontent), 'utf8', (err) => { 
      if(err) console.error('Error writing the file ', err)
    })
    console.log(' file ', filecontent)
    res.json(filecontent)
    res.end()
  }
})

router.post('/filecontent', (req, res, next) => {
  //console.log(req.body.filename)
  fs.readFile(path.join(__dirname, req.body.filename), (err, data) => {  
    //console.log(err, data)
    if (err) {
      res.end()
    } else {
      let parsed = JSON.parse(data)
      //console.log(parsed)
      res.json(parsed)
      res.end()
    }
    
  })
})


router.use('/analyse', analyse)

export default router
